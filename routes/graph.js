const express = require('express')
const graphqlHTTP = require('express-graphql')

const schemaData = require('../Schema')

const router = express.Router()

const getAuthTokenFromRequest = (req) => {
	const authHeader = req.headers.authorization
	if (authHeader && authHeader.match(/^bearer /i))
		return authHeader.split(/\s/)[1]
}

router.use('/', graphqlHTTP((req) => {
	const authToken = getAuthTokenFromRequest(req)
	return ({
		schema: schemaData.Schema,
		context: {
			loaders: schemaData.createLoaders(authToken),
			authToken
		},
		graphiql: true
	})
}))

module.exports = router