const {
	GraphQLObjectType,
	GraphQLSchema,
	GraphQLString,
	GraphQLInt,
	GraphQLNonNull,
	GraphQLList,
	GraphQLID
} = require('graphql')
const {GraphQLDateTime} = require('graphql-iso-date')

const User = require('../User')
const Post = require('../Post')

const CommentType = new GraphQLObjectType({
	name: 'Comment',
	fields: () => ({
		post: {
			type: new GraphQLNonNull(Post.Type),
			resolve: (comment, param, context) => context.loaders.postsBulk.load(comment.postId)
		},
		date: {type: new GraphQLNonNull(GraphQLDateTime)},
		author: {
			type: new GraphQLNonNull(User.Type),
			resolve: (comment) => ({name: comment.author})
		},
		comment: {type: new GraphQLNonNull(GraphQLString)}
	})
})

module.exports = CommentType