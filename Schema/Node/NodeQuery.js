const {
	GraphQLObjectType,
	GraphQLSchema,
	GraphQLString,
	GraphQLInt,
	GraphQLNonNull,
	GraphQLList,
	GraphQLID
} = require('graphql')

const nodeDef = require('./NodeDefinitions')

module.exports = {
	node: nodeDef.nodeField
}
