// schema.js
const {
	GraphQLObjectType,
	GraphQLSchema,
	GraphQLString,
	GraphQLInt,
	GraphQLNonNull,
	GraphQLList,
	GraphQLID
} = require('graphql')

const Post = require('./Post')
const PostList = require('./PostList')
const Comment = require('./Comment')
const Like = require('./Like')
const Tag = require('./Tag')
const User = require('./User')
const Node = require('./Node')

const RootQuery = new GraphQLObjectType({
	name: 'Query',
	fields: () => ({
		...Node.Queries,
		...Post.Queries,
		...PostList.Queries,
		...Tag.Queries,
		...User.Queries
	})
})

const RootMutation = new GraphQLObjectType({
	name: 'Mutation',
	fields: () => ({
		...Post.Mutations,
		...Comment.Mutations
	})
})

const schema = new GraphQLSchema({
	query: RootQuery,
	mutation: RootMutation
})

const createLoaders = (authToken) => {
	return {
		commentByPostLoader: Comment.Loaders.commentByPostLoader(authToken),
		commentsByAuthorLoader: Comment.Loaders.commentsByAuthorLoader(authToken),
		postsBulk: Post.Loaders.postsBulk(authToken),
		likesByUserLoader: Like.Loaders.likesByUserLoader(authToken)
	}
}

module.exports = {Schema: schema, createLoaders}
