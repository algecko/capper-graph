const {globalIdField, fromGlobalId} = require('graphql-relay')

const PostType = require('./PostType')
const PostDal = require('./PostDal')

module.exports =  {
	likePost: {
		type: PostType,
		args: {
			postId: globalIdField('Post')
		},
		resolve: (root, {postId}) => PostDal.like(fromGlobalId(postId).id)
	},

	unLikePost: {
		type: PostType,
		args: {
			postId: globalIdField('Post')
		},
		resolve: (root, {postId}) => PostDal.unlike(fromGlobalId(postId).id)
	}
}