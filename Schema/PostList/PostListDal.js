const fetch = require('node-fetch')
const config = require('../../config')

const getCursor = (post, options) => Buffer.from(
	options.sort === 'best' ?
		('' + post.likesWithDate) :
		post.date.toISOString())
	.toString('base64')

const mapPostToEdge = (options) => (post) => {
	return {node: post, cursor: getCursor(post, options)}
}

const preparePost = (post) => ({
	...post,
	date: new Date(post.date)
})

const buildPageInfo = (posts, first, last, options) => ({
	startCursor: getCursor(posts[0], options),
	endCursor: getCursor(posts[posts.length - 1], options),
	hasNextPage: !last || !posts.find(post => post.id === last),
	hasPreviousPage: !first || !posts.find(post => post.id === first)
})

const preparePostListForRelay = (options) => (postData) => {
	if (!postData)
		return

	const posts = postData.posts.map(preparePost)

	return {
		edges: posts.map(mapPostToEdge(options)),
		pageInfo: buildPageInfo(posts, postData.first, postData.last, options)
	}
}

const parseCursor = (cursor, options) => options.sort === 'best' ?
	parseInt(Buffer.from(cursor, 'base64').toString(), 10) :
	new Date(Buffer.from(cursor, 'base64').toString())

const decodeOptions = (options) => {
	const decoded = {...options}
	if (decoded.after)
		decoded.after = parseCursor(decoded.after, options)

	if (decoded.before)
		decoded.before = parseCursor(decoded.before, options)

	Object.keys(decoded).forEach(key => {
		if (!decoded[key])
			delete decoded[key]
	})
	return decoded
}

module.exports.findPosts = (options, authToken) => fetch(`${config.postServerUrl}postSearch`, {
	method: 'POST',
	headers: {
		'content-type': 'application/json',
		'authorization': `Bearer ${authToken}`
	},
	body: JSON.stringify(decodeOptions(options))
})
	.then(res => res.json())
	.then(preparePostListForRelay(options))