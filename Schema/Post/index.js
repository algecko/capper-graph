const dal = require('./PostDal')

module.exports.Type = require('./PostType')
module.exports.Queries = require('./PostQueries')
module.exports.Loaders = require('./PostLoaders')
module.exports.Mutations = require('./PostMutations')
module.exports.PublicDal = {
	getAllAuthors: dal.getAllAuthors,
	getAllTags: dal.getAllTags,
	getPost: dal.getPostById
}