const fetch = require('node-fetch')
const config = require('../../config')

module.exports.getAll = (options) => sendJSON(`${config.postServerUrl}postSearch`, 'POST', options)

module.exports.getPostsById = (ids) => sendJSON(`${config.postServerUrl}postSearch/byId`, 'POST', {ids}).then(res => res.json())

module.exports.getPostById = (id) => fetch(`${config.postServerUrl}posts/${id}`).then(res => res.json())

module.exports.like = (id) => fetch(`${config.postServerUrl}posts/${id}/like`, {method: 'PUT'}).then(res => res.json())

module.exports.unlike = (id) => fetch(`${config.postServerUrl}posts/${id}/unlike`, {method: 'PUT'}).then(res => res.json())

module.exports.getPostsPerDate = () => fetch(`${config.postServerUrl}meta/postsPerDate`).then(res => res.json())

// --- begin Public queries
module.exports.getAllAuthors = () => fetch(`${config.postServerUrl}authors`).then(res => res.json())
module.exports.getAllTags = () => fetch(`${config.postServerUrl}tags`).then(res => res.json())
// --- end Public queries

const sendJSON = (url, method, data) => fetch(url, {
	method,
	headers: {
		'content-type': 'application/json'
	},
	body: JSON.stringify(data)
})