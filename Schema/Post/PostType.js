const {
	GraphQLObjectType,
	GraphQLString,
	GraphQLInt,
	GraphQLNonNull,
	GraphQLList,
	GraphQLBoolean
} = require('graphql')
const {GraphQLDateTime} = require('graphql-iso-date')
const {globalIdField, connectionArgs} = require('graphql-relay')

const Comment = require('../Comment')
const User = require('../User')
const Tag = require('../Tag')
const Node = require('../Node')

module.exports = new GraphQLObjectType({
	name: 'Post',
	fields: () => ({
		id: globalIdField('Post'),
		author: {
			type: new GraphQLNonNull(User.Type),
			resolve: (post) => ({name: post.author})
		},
		text: {type: new GraphQLNonNull(GraphQLString)},
		imgUrl: {type: GraphQLString},
		title: {type: new GraphQLNonNull(GraphQLString)},
		//todo make date scalar type
		date: {type: new GraphQLNonNull(GraphQLDateTime)},
		tags: {
			type: new GraphQLList(Tag.Type),
			resolve: (post, args, context) => post.tags.map(tag => ({name: tag}))
		},
		noImg: {type: GraphQLBoolean},
		imgDead: {type: GraphQLBoolean},
		likes: {type: GraphQLInt},
		size: {type: GraphQLInt},
		comments: {
			type: Comment.ConnectionDefinition.connectionType,
			args: connectionArgs,
			resolve: (post, args, context) => context.loaders.commentByPostLoader.load(post.id)
		}
	}),
	interfaces: [Node.Interface]
})
