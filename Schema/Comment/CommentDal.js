const fetch = require('node-fetch')
const config = require('../../config')

module.exports.getBulkForPosts = (postIds) => {
	return commentFetch(`${config.commentServerUrl}bulk`, postIds)
}

module.exports.getBulkForAuthors = (authors) => {
	return commentFetch(`${config.commentServerUrl}bulkByAuthor`, authors)
}

module.exports.createComment = (postId, comment, auth) =>
	commentFetch(`${config.commentServerUrl}post/${postId}/comments`, comment, auth)

const commentFetch = (resource, body, auth) => {
	const headers = {
		'content-type': 'application/json'
	}
	if (auth)
		headers.authorization = `Bearer ${auth}`
	return fetch(resource, {
		method: 'POST',
		headers,
		body: JSON.stringify(body)
	})
	.then(res => {
		if (res.status === 401)
			throw new Error('Unauthorized')
		return res.json()
	})
}
