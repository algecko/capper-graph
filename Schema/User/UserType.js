const {
	GraphQLObjectType,
	GraphQLSchema,
	GraphQLString,
	GraphQLInt,
	GraphQLNonNull,
	GraphQLList,
	GraphQLID
} = require('graphql')

const {connectionDefinitions, connectionArgs} = require('graphql-relay')
const Post = require('../Post')
const Comment = require('../Comment')
const Like = require('../Like')
const {SortType} = require('../PostList')

module.exports = new GraphQLObjectType({
	name: 'User',
	fields: () => ({
		name: {type: new GraphQLNonNull(GraphQLString)},
		comments: {
			type: new GraphQLList(Comment.Type),
			resolve: (author, param, context) => context.loaders.commentsByAuthorLoader.load(author.name)
		},
		likes: {
			type: new GraphQLList(Like.Type),
			resolve: (author, param, context) => context.loaders.likesByUserLoader.load(author.name)
		}
	})
})
