const {
	GraphQLObjectType,
	GraphQLSchema,
	GraphQLString,
	GraphQLInt,
	GraphQLNonNull,
	GraphQLList,
	GraphQLID
} = require('graphql')

const Type = require('./UserType')
const Post = require('../Post')

module.exports = {
	user: {
		type: Type,
		args: {
			name: {
				type: new GraphQLNonNull(GraphQLString)
			}
		},
		resolve: (root, {name}) => ({name})
	},
	authors: {
		type: new GraphQLList(Type),
		resolve: () => Post.PublicDal.getAllAuthors()
	}
}
