module.exports.Type = require('./CommentType')
module.exports.Loaders = require('./CommentLoaders')
module.exports.Mutations = require('./CommentMutations')
module.exports.ConnectionDefinition = require('./CommentConnectionDefinition')