const postDal = require('./PostDal')
const DataLoader = require('dataloader')

module.exports.postsBulk = (auth) =>
	new DataLoader(postIds => postDal.getPostsById(postIds)
		.then(posts => postIds.map(postId => posts.find(post => post.id === postId)))
	)