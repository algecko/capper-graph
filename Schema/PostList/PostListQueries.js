const {
	GraphQLObjectType,
	GraphQLSchema,
	GraphQLString,
	GraphQLInt,
	GraphQLNonNull,
	GraphQLList,
	GraphQLID
} = require('graphql')

const Type = require('./PostListType')

module.exports = {
	reader: {
		type: Type,
		resolve: () => ({id: '1'})
	}
}
