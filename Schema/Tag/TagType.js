const {
	GraphQLObjectType,
	GraphQLSchema,
	GraphQLString,
	GraphQLInt,
	GraphQLNonNull,
	GraphQLList,
	GraphQLID
} = require('graphql')
const {connectionDefinitions, connectionArgs} = require('graphql-relay')

const Post = require('../Post')
const {SortType} = require('../PostList')

module.exports = new GraphQLObjectType({
	name: 'Tag',
	fields: () => ({
		name: {type: new GraphQLNonNull(GraphQLString)},
		postCount: {type: GraphQLInt}
	})
})
