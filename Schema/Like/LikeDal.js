const fetch = require('node-fetch')

module.exports.getBulkLikedPosts = (userNames, token) => fetch('http://localhost:3090/postLikes', {
	headers: {
		authorization: `Bearer ${token}`
	}
})
.then(res => res.json())
.catch(() => [])
.then(likes => likes.map(like => ({user: like.user, post: like.postId})))