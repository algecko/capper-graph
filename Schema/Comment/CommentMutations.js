const {
	GraphQLObjectType,
	GraphQLSchema,
	GraphQLString,
	GraphQLInt,
	GraphQLNonNull,
	GraphQLList,
	GraphQLID,
	GraphQLBoolean
} = require('graphql')
const {fromGlobalId} = require('graphql-relay')
const createCursor = require('./createCursor')

const CommentType = require('./CommentType')
const {NewComment} = require('./CommentInputTypes')
const EdgeType = require('./CommentConnectionDefinition').edgeType
const CommentDal = require('./CommentDal')

module.exports = {
	newComment: {
		type: new GraphQLObjectType({
			name: 'NewCommentResponse',
			fields: () => ({
				newCommentEdge: {
					type: EdgeType
				}
			})
		}),
		args: {
			comment: {type: new GraphQLNonNull(NewComment)}
		},
		resolve: (root, {comment}, context) => {
			const postId = fromGlobalId(comment.postId).id
			return CommentDal.createComment(postId, {
				...comment,
				postId
			}, context.authToken)
			.then(comment => ({
				newCommentEdge: {
					node: comment,
					cursor: createCursor(comment)
				}
			}))
		}
	}
}