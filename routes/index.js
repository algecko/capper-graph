const express = require('express')
const cors = require('cors')

const errorHandling = require('./errorHandling')
const graphRoute = require('./graph')

const app = express()
app.use(cors())

app.use('/graph', graphRoute)

errorHandling(app)

module.exports = app