const {
	GraphQLObjectType,
	GraphQLSchema,
	GraphQLString,
	GraphQLInt,
	GraphQLNonNull,
	GraphQLList,
	GraphQLID
} = require('graphql')

const Type = require('./TagType')
const Post = require('../Post')

module.exports = {
	tag: {
		type: Type,
		args: {
			name: {
				type: new GraphQLNonNull(GraphQLString)
			}
		},
		resolve: (root, {name}) => ({name})
	},
	tags: {
		type: new GraphQLList(Type),
		resolve: () => Post.PublicDal.getAllTags()
	}
}
