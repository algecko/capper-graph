const commentDal = require('./CommentDal')
const DataLoader = require('dataloader')
const createCursor = require('./createCursor')

module.exports.commentByPostLoader = (auth) =>
	new DataLoader(keys => commentDal.getBulkForPosts(keys)
		.then(comments => keys.map(postId => ({
			pageInfo: {hasNextPage: false, hasPreviousPage: false},
			edges: comments.filter(comment => comment.postId === postId)
			.map(comment => ({node: comment, cursor: createCursor(comment)}))
		})))
	)

module.exports.commentsByAuthorLoader = (auth) => {
	return new DataLoader(keys => commentDal.getBulkForAuthors(keys)
		.then(comments => keys.map(author => comments.filter(comment => comment.author === author)))
	)
}