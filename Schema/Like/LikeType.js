const {
	GraphQLObjectType,
	GraphQLSchema,
	GraphQLString,
	GraphQLInt,
	GraphQLNonNull,
	GraphQLList,
	GraphQLID
} = require('graphql')

const User = require('../User')
const Post = require('../Post')

module.exports = new GraphQLObjectType({
	name: 'Like',
	fields: () => ({
		user: {
			type: new GraphQLNonNull(User.Type),
			resolve: (comment) => ({name: comment.user})
		},
		post: {
			type: new GraphQLNonNull(Post.Type),
			resolve: (comment, args, context) => context.loaders.postsBulk.load(comment.post)
		}
	})
})
