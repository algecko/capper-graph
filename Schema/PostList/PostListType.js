const {
	GraphQLObjectType,
	GraphQLString,
	GraphQLInt,
	GraphQLNonNull,
	GraphQLList,
	GraphQLID,
	GraphQLBoolean
} = require('graphql')
const {connectionDefinitions, connectionArgs} = require('graphql-relay')

const Post = require('../Post')
const SortType = require('./SortType')
const dal = require('./PostListDal')

module.exports = new GraphQLObjectType({
	name: 'PostList',
	fields: () => ({
		id: {type: new GraphQLNonNull(GraphQLID)},
		posts: {
			type: connectionDefinitions({
				nodeType: Post.Type
			}).connectionType,
			args: {
				tags: {
					type: new GraphQLList(GraphQLString),
					description: 'Limit results to include certain tags'
				},
				authors: {
					type: new GraphQLList(GraphQLString),
					description: 'Limit results to include certain authors'
				},
				sort: {
					type: SortType
				},
				dateRange: {type: GraphQLString},
				text: {type: GraphQLString},
				likedByMe: {type: GraphQLBoolean},
				minSize: {type: GraphQLInt},
				maxSize: {type: GraphQLInt},
				...connectionArgs
			},
			resolve: (root, args, context) => dal.findPosts(args, context.authToken)
		}
	})
})
