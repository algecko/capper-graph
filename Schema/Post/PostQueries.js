const {
	GraphQLObjectType,
	GraphQLSchema,
	GraphQLString,
	GraphQLInt,
	GraphQLNonNull,
	GraphQLList,
	GraphQLID
} = require('graphql')
const {GraphQLDate} = require('graphql-iso-date')

const {fromGlobalId} = require('graphql-relay')

const PostType = require('./PostType')
const Post = require('./PostDal')

module.exports = {
	post: {
		type: PostType,
		args: {
			id: {
				type: new GraphQLNonNull(GraphQLID)
			}
		},
		resolve: (root, {id}) => Post.getPostById(fromGlobalId(id).id)
	},
	postsPerDate: {
		type: new GraphQLList(new GraphQLObjectType({
			name: 'PostsPerDate',
			fields: () => ({
				day: {type: new GraphQLNonNull(GraphQLDate)},
				postCount: {type: new GraphQLNonNull(GraphQLInt)}
			})
		})),
		resolve: () => Post.getPostsPerDate()
	}
}
