const LikeDal = require('./LikeDal')
const DataLoader = require('dataloader')

module.exports.likesByUserLoader = (auth) => {
	return new DataLoader(userNames => LikeDal.getBulkLikedPosts(userNames, auth)
		.then(likes => userNames.map(userName => likes.filter(like => like.user === userName)))
	)
}