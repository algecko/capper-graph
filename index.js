const app = require('./routes')

const PORT = process.env.PORT || 3051
app.listen(PORT, function () {
	console.log(`Server listening at ${PORT}`)
})
