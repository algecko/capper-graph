const {nodeDefinitions, fromGlobalId} = require('graphql-relay')

const Post = require('../Post')

const POST_TYPE = 'Post'

module.exports = nodeDefinitions(
	(globalId) => {
		const {type, id} = fromGlobalId(globalId)
		if (type === POST_TYPE)
			return Post.PublicDal.getPost(id)
	},
	(obj) => {
		if (obj.title && obj.text && obj.author && obj.date)
			return POST_TYPE
	}
)