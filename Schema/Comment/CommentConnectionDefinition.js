const {connectionDefinitions} = require('graphql-relay')

const Type = require('./CommentType')

module.exports = connectionDefinitions({
	nodeType: Type
})

