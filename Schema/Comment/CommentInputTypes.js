const {
	GraphQLInputObjectType,
	GraphQLSchema,
	GraphQLString,
	GraphQLInt,
	GraphQLNonNull,
	GraphQLList,
	GraphQLID
} = require('graphql')

module.exports.NewComment = new GraphQLInputObjectType({
	name: 'NewComment',
	fields: () => ({
		comment: {type: new GraphQLNonNull(GraphQLString)},
		postId: {type: new GraphQLNonNull(GraphQLString)},
		postDate: {type: new GraphQLNonNull(GraphQLString)}
	})
})
