const {GraphQLEnumType} = require('graphql')

module.exports = new GraphQLEnumType({
	name: 'SortModes',
	values: {
		new: {
			value: 'new',
			description: 'Newest posts first (default)'
		},
		best: {
			value: 'best',
			description: 'Best posts first'
		},
		random: {
			value: 'random',
			description: 'Random order'
		}
	}
})